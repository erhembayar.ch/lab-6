package shop.products.service;

import jms.OrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import shop.products.domain.Product;
import shop.products.domain.Stock;
import shop.products.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Component
public class MessageListener {

    @Autowired
    ProductRepository productRepository;


    @JmsListener(destination = "order")
    public void receiveMessage(final OrderDTO order) {
        System.out.println("JMS receiver received message:" + order.toString());

        List<OrderLineDTO> orderList = order.getOrderlineList();
        for(OrderLineDTO orderLineDTO: orderList){
            ProductDTO productDTO = orderLineDTO.getProduct();
            Optional<Product> product = productRepository.findById(productDTO.getProductnumber());

            if(product.get() !=null){
                Stock stock = product.get().getStock();
                stock.setQuantity(stock.getQuantity() - orderLineDTO.getQuantity());

                product.get().setStock(stock);
                productRepository.save(product.get());
                System.out.println("JMS receiver received message: Stock Updated" + order.toString());

            }

        }

    }
}
