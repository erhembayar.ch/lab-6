package shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;
import shop.service.*;


@SpringBootApplication
public class WebShopApplication implements CommandLineRunner {
	@Autowired
	private RestOperations  restTemplate;

	public static void main(String[] args) {
		SpringApplication.run(WebShopApplication.class, args);
	}

	@Bean
	RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		return restTemplate;
	}

	@Override
	public void run(String... args) throws Exception {
		//create customer
		CustomerDTO customerDto = new CustomerDTO("101","Frank","Brown","fBrown@Hotmail.com","123456");
		AddressDTO addressDTO = new AddressDTO("1000 N main Street", "Fairfield","52557","USA");
		customerDto.setAddress(addressDTO);

		final String urlCustomers = "http://localhost:8091";
		final String urlProducts = "http://localhost:8092";
		final String urlShopping = "http://localhost:8093";
		final String urlOrder = "http://localhost:8094";

		//todo: call the customer component to add the customer
		restTemplate.postForObject(urlCustomers + "/customer", customerDto, CustomerDTO.class);

		// get customer
		//todo: call the customer component to get the customer with id 101 and print the result
		CustomerDTO cust = restTemplate.getForObject(urlCustomers + "/customer/101", CustomerDTO.class);
		System.out.println("Customer with id 101: " + cust.toString());

		//create products
		//todo: call the product component to create the first product 
		//todo: call the product component to create the second product 
//		ProductDTO prod1 = new ProductDTO("1", "Product 1", 100);
		restTemplate.postForObject(urlProducts + "/product/1/Product 1/100" , null, ProductDTO.class);
		restTemplate.postForObject(urlProducts + "/product/2/Product 2/200", null, ProductDTO.class);

		//set stock	
		//todo: call the product component to set the stock for the first product
		restTemplate.postForObject(urlProducts + "/product/stock/1/10/loc1" , null, ProductDTO.class);
		restTemplate.postForObject(urlProducts + "/product/stock/2/20/loc2" , null, ProductDTO.class);

		//get a product
		//todo: call the product component to get the the first product and print the result
		ProductDTO prod1 = restTemplate.getForObject(urlProducts + "/product/1", ProductDTO.class);
		prod1.print();

		// add products to the shoppingcart
		//todo: call the shopping component to add the first product to the cart
		//todo: call the shopping component to add the second product to the cart

		restTemplate.postForObject(urlShopping + "/cart/1/1/1" , null, ShoppingCartDTO.class);
		restTemplate.postForObject(urlShopping + "/cart/1/2/2" , null, ShoppingCartDTO.class);

		//get the shoppingcart
		//todo: call the shopping component to get the cart and print the result
		ShoppingCartDTO cart = restTemplate.getForObject(urlShopping + "/cart/1", ShoppingCartDTO.class);
		cart.print();

		//checkout the cart		
		//todo: call the shopping component to checkout the cart 
		restTemplate.postForObject(urlShopping + "/cart/checkout/1" , null, ShoppingCartDTO.class);

		//get the order
		//todo: call the order component to get the order and print the result 
		OrderDTO order = restTemplate.getForObject(urlOrder + "/order/1", OrderDTO.class);
		order.print();

		//add customer to order
		//todo: call the order component to add a customer to the order
		restTemplate.postForObject(urlOrder + "/order/setCustomer/1/101" , null, OrderDTO.class);


		//confirm the order
		//todo: call the order component to confirm the order
		restTemplate.postForObject(urlOrder + "/order/1" , null, OrderDTO.class);

		//get the order
		//todo: call the order component to get the order and print the result
		OrderDTO orderFinal = restTemplate.getForObject(urlOrder + "/order/1", OrderDTO.class);
		System.out.println("Final order: ");
		orderFinal.print();
	}


}
